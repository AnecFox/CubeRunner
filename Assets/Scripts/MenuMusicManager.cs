using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuMusicManager : MonoBehaviour
{
	[SerializeField] private static MenuMusicManager _instance = null;
	[SerializeField] private AudioSource _audio;

	private void Start()
	{
		_audio = GetComponent<AudioSource>();
		_audio.Play();
	}

	private void Update()
	{
		if (SceneManager.GetActiveScene().buildIndex != 0 && SceneManager.GetActiveScene().buildIndex != 1)
		{
			_audio.Stop();
		}
		else if (!_audio.isPlaying)
		{
			_audio.Play();
		}
	}

	private void Awake()
	{
		if (_instance == null)
		{
			_instance = this;
			DontDestroyOnLoad(gameObject);
			return;
		}

		if (_instance == this)
			return;

		Destroy(gameObject);
	}
}
