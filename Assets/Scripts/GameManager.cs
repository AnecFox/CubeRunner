using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public PlayerMovement movement;
    public Pause pause;
    
    public float levelRestartDelay = 2f;

    public void EndGame()
    {
        movement.enabled = false;
        pause.enabled = false;

        Invoke(nameof(RestartLevel), levelRestartDelay);
    }

    private void RestartLevel() => SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

    public void ChangeScene(int sceneNumber) => SceneManager.LoadScene(sceneNumber);

    public void NextLevel(bool takeCursorVisible = false)
    {
        ChangeScene(SceneManager.GetActiveScene().buildIndex + 1);
        
        if (takeCursorVisible)
            Cursor.lockState = CursorLockMode.Confined;
    }

    public void SetPause(bool pauseEnabled) => movement.enabled = !pauseEnabled;

    public void Exit() => Application.Quit();
}
