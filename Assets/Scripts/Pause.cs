using UnityEngine;

public class Pause : MonoBehaviour
{
    public PlayerMovement movement;

    public GameManager gm;

    public GameObject pressSpaceToStart;
    public GameObject pauseMenu;

    private bool _pauseEnabled = true;

    private void Start()
    {
        pauseMenu.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Escape) && !pressSpaceToStart.activeSelf && !pauseMenu.activeSelf)
        {
            pauseMenu.SetActive(true);
            _pauseEnabled = true;
            Cursor.lockState = CursorLockMode.Confined;
        }
        else if (Input.GetKey(KeyCode.Space))
        {
            pressSpaceToStart.SetActive(false);
            pauseMenu.SetActive(false);
            _pauseEnabled = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
    }

    private void FixedUpdate() => gm.SetPause(_pauseEnabled);
}
