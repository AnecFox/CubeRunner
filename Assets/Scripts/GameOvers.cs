using UnityEngine;

public class GameOvers : MonoBehaviour
{
    public GameManager gm;

    [SerializeField] private float _minHeight = -5f;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Obstacle"))
        {
            gm.EndGame();
            Debug.Log("Game Over!");
        }
        else if (collision.collider.CompareTag("NextLevel"))
        {
            gm.NextLevel(takeCursorVisible: true);
            Debug.Log("Level is passed!");
        }
    }

    private void Update()
    {
        if (transform.position.y < _minHeight)
        {
            gm.EndGame();
            Debug.Log("Game Over!");
        }
    }
}
