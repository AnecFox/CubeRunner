using UnityEngine;
using DG.Tweening;

public class PlayerMovement : MonoBehaviour
{
    public Rigidbody rb;

    [SerializeField] private float runSpeed = 15f;
    [SerializeField] private float strafeSpeed = 14f;
    [SerializeField] private float jumpForce = 16f;

    private bool _strafeLeft = false;
    private bool _strafeRight = false;

    private bool _doJump;

    private bool _isGrounded;

    private void OnCollisionStay() => _isGrounded = true;

    private void Update()
    {
        _strafeLeft = Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow);
        _strafeRight = Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            _doJump = true;
        }
    }

    private void FixedUpdate()
    {
        rb.MovePosition(transform.position + runSpeed * Time.deltaTime * Vector3.forward);

        if (_strafeLeft)
            rb.MovePosition(transform.position + strafeSpeed * Time.deltaTime * Vector3.left);

        else if (_strafeRight)
            rb.MovePosition(transform.position + strafeSpeed * Time.deltaTime * Vector3.right);

        if (_doJump && _isGrounded)
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);

            transform.DORewind();
            transform.DOShakeScale(.5f, .5f, 3, 30);

            _doJump = _isGrounded = false;
        }
    }
}
